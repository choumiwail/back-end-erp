package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "rendezsvous")
public class RendezVous implements Serializable {
	
	@Id
	@Column(name = "nrdv", nullable = false)
		private String nrdv;
	@Temporal(TemporalType.DATE)
		private Date daterdv;
	@Temporal(TemporalType.DATE)
	    private Date datecreation;
	    private String avis;
	    private String description;
	
	@OneToMany(mappedBy="rdv",fetch=FetchType.LAZY)
		private Collection<Suivi> suivi;

	public String getNrdv() {
		return nrdv;
	}

	public void setNrdv(String nrdv) {
		this.nrdv = nrdv;
	}

	public Date getDaterdv() {
		return daterdv;
	}

	public void setDaterdv(Date daterdv) {
		this.daterdv = daterdv;
	}

	public Date getDatecreation() {
		return datecreation;
	}

	public void setDatecreation(Date datecreation) {
		this.datecreation = datecreation;
	}

	public String getAvis() {
		return avis;
	}

	public void setAvis(String avis) {
		this.avis = avis;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	/*public Collection<Suivi> getSuivi() {
		return suivi;
	}*/

	public void setSuivi(Collection<Suivi> suivi) {
		this.suivi = suivi;
	}

	public RendezVous() {
		super();
	}

	public RendezVous(String nrdv) {
		super();
		this.nrdv = nrdv;
	}

	public RendezVous(String nrdv, Date daterdv, Date datecreation, String avis, String description) {
		super();
		this.nrdv = nrdv;
		this.daterdv = daterdv;
		this.datecreation = datecreation;
		this.avis = avis;
		this.description = description;
	}


  
	
	

}
