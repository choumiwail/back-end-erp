package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "clients")
public class Client implements Serializable {
	
	@Id
	@Column(name = "matriculeclient", nullable = false)
		private String matriculeclient;
		private String nom;
		private String prenom;
		private String fonction;
		private String adresse;
		private int codepostal;
		private String ville;
		private String pays;
		private String email;
		private String email1;
		private long tel;
		private long tel1;
	@Temporal(TemporalType.DATE)
		private Date datecreation;
		private String etat;
		private String secteur;
	    private String direction;
		private String hierarchie;
		private String hierarchis;
		private String ficheclient;
		private String activiteclient;
		private long portableperso;
		private long portablepro;
	
	@ManyToMany
	@JoinTable(name="employeursclients")
		private Collection<Employeur> employeurs;	
	/*@OneToMany(mappedBy="client",fetch=FetchType.LAZY)
	private Collection<Acheteur> acheteurs;*/
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="codeacheteur")
		private Acheteur acheteur;
	@ManyToOne
	@JoinColumn(name="codecompte")
		private Compte compte;
	@OneToMany(mappedBy="client",fetch=FetchType.LAZY)
	private Collection<Suivi> suivis;
	@OneToMany(mappedBy="client",fetch=FetchType.LAZY)
	private Collection<Offre> offres;
	@OneToMany(mappedBy="client",fetch=FetchType.LAZY)
	private Collection<Contrat> contrat;
	
	
	public String getMatriculeclient() {
		return matriculeclient;
	}
	public void setMatriculeclient(String matriculeclient) {
		this.matriculeclient = matriculeclient;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getFonction() {
		return fonction;
	}
	public void setFonction(String fonction) {
		this.fonction = fonction;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public int getCodepostal() {
		return codepostal;
	}
	public void setCodepostal(int codepostal) {
		this.codepostal = codepostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail1() {
		return email1;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	public long getTel() {
		return tel;
	}
	public void setTel(long tel) {
		this.tel = tel;
	}
	public long getTel1() {
		return tel1;
	}
	public void setTel1(long tel1) {
		this.tel1 = tel1;
	}
	public Date getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Date datecreation) {
		this.datecreation = datecreation;
	}
	public String getDirection() {
		return direction;
	}
	public void setDirection(String direction) {
		this.direction = direction;
	}
	public String getHierarchie() {
		return hierarchie;
	}
	public void setHierarchie(String hierarchie) {
		this.hierarchie = hierarchie;
	}
	public String getHierarchis() {
		return hierarchis;
	}
	public void setHierarchis(String hierarchis) {
		this.hierarchis = hierarchis;
	}
	/*public Collection<Employeur> getEmployeurs() {
		return employeurs;
	}*/
	public void setEmployeurs(Collection<Employeur> employeurs) {
		this.employeurs = employeurs;
	}
	
	public String getEtat() {
		return etat;
	}
	public void setEtat(String etat) {
		this.etat = etat;
	}
	public String getFicheclient() {
		return ficheclient;
	}
	public void setFicheclient(String ficheclient) {
		this.ficheclient = ficheclient;
	}
	public String getActiviteclient() {
		return activiteclient;
	}
	public void setActiviteclient(String activiteclient) {
		this.activiteclient = activiteclient;
	}
	public Acheteur getAcheteur() {
		return acheteur;
	}
	public void setAcheteur(Acheteur acheteur) {
		this.acheteur = acheteur;
	}
	
	public Compte getCompte() {
		return compte;
	}
	public void setCompte(Compte compte) {
		this.compte = compte;
	}
	
	
	public void setSuivis(Collection<Suivi> suivis) {
		this.suivis = suivis;
	}
	
	public String getSecteur() {
		return secteur;
	}
	public void setSecteur(String secteur) {
		this.secteur = secteur;
	}
	
	
	public long getPortableperso() {
		return portableperso;
	}
	public void setPortableperso(long portableperso) {
		this.portableperso = portableperso;
	}
	public long getPortablepro() {
		return portablepro;
	}
	public void setPortablepro(long portablepro) {
		this.portablepro = portablepro;
	}
	
	public void setContrat(Collection<Contrat> contrat) {
		this.contrat = contrat;
	}
	public Client() {
		super();
	}
	public Client(String matriculeclient) {
		super();
		this.matriculeclient = matriculeclient;
	}
	public void setOffres(Collection<Offre> offres) {
		this.offres = offres;
	}
	
	
	
	
	

}
