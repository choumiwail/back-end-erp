package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "sousentites")
public class SousEntite implements Serializable{
	
	@Id
	@Column(name = "nsentite", nullable = false)
		private String nsentite;
		private String nomsentite;
	@Temporal(TemporalType.DATE)
		private Date datecreation;
		private String secteur;
		private String adresse;
		private String ville;
		private int codepostal;
		private String pays;
		private long tel;
		private long tel1;
		private boolean etat;
	
	@ManyToOne
	@JoinColumn(name="nentiter")
	private Entite entite;	
	@ManyToOne
	@JoinColumn(name="codecompter")
	private Compte compte;
	@ManyToMany
	@JoinTable(name="entiteemployeur")
	private Collection<Employeur> employeur;
	@OneToMany(mappedBy="sousentite",fetch=FetchType.LAZY)
	private Collection<Offre> offres;
	
	public String getNsentite() {
		return nsentite;
	}
	public void setNsentite(String nsentite) {
		this.nsentite = nsentite;
	}
	public String getNomsentite() {
		return nomsentite;
	}
	public void setNomsentite(String nomsentite) {
		this.nomsentite = nomsentite;
	}
	public Date getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Date datecreation) {
		this.datecreation = datecreation;
	}
	public String getSecteur() {
		return secteur;
	}
	public void setSecteur(String secteur) {
		this.secteur = secteur;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public int getCodepostal() {
		return codepostal;
	}
	public void setCodepostal(int codepostal) {
		this.codepostal = codepostal;
	}
	public long getTel() {
		return tel;
	}
	public void setTel(long tel) {
		this.tel = tel;
	}
	
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public long getTel1() {
		return tel1;
	}
	public void setTel1(long tel1) {
		this.tel1 = tel1;
	}
	public boolean isEtat() {
		return etat;
	}
	public void setEtat(boolean etat) {
		this.etat = etat;
	}
	public Entite getEntite() {
		return entite;
	}
	public void setEntite(Entite entite) {
		this.entite = entite;
	}
	public Compte getCompte() {
		return compte;
	}
	public void setCompte(Compte compte) {
		this.compte = compte;
	}
	public Collection<Employeur> getEmployeur() {
		return employeur;
	}
	public void setEmployeur(Collection<Employeur> employeur) {
		this.employeur = employeur;
	}
	
	
	
	public void setOffres(Collection<Offre> offres) {
		this.offres = offres;
	}
	public SousEntite() {
		super();
	}
	public SousEntite(String nsentite) {
		super();
		this.nsentite = nsentite;
	}
	public SousEntite(String nsentite, String nomsentite, Date datecreation, String secteur, String adresse,
			String ville, int codepostal, String pays, long tel, long tel1, boolean etat, Entite entite,
			Compte compte) {
		super();
		this.nsentite = nsentite;
		this.nomsentite = nomsentite;
		this.datecreation = datecreation;
		this.secteur = secteur;
		this.adresse = adresse;
		this.ville = ville;
		this.codepostal = codepostal;
		this.pays = pays;
		this.tel = tel;
		this.tel1 = tel1;
		this.etat = etat;
		this.entite = entite;
		this.compte = compte;
	}
	
	
	
	
	
	
	

}
