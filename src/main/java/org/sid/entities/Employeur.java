package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.context.annotation.Scope;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "employeurs")
@Scope("session")
public class Employeur implements Serializable,UserDetails{
	
	@Id
	@Column(name = "username", nullable = false)
		private String username;
		private String nom;
		private String prenom;
		private String password;
	@Temporal(TemporalType.DATE)
		private Date datecreation;
		private String typeemp; 
	@Temporal(TemporalType.DATE)
		private Date datemodication;
		private String email;
		private String service;
		
	@ManyToOne
	@JoinColumn(name="reffilialer")
		private Filiale filiale;
	@ManyToMany
	@JoinTable(name="employeursco")
	private Collection<Compte> comptes;
	@ManyToMany
	@JoinTable(name="employeursen")
	private Collection<Entite> entites;
	@ManyToMany
	@JoinTable(name="employeursse")
	private Collection<SousEntite> sousentites;
	@ManyToMany
	@JoinTable(name="Employeurscl")
	private Collection<Client> clients;
	@OneToMany(mappedBy="employeur",fetch=FetchType.LAZY)
	private Collection<Suivi> suivi;
	@OneToMany(mappedBy="employeur",fetch=FetchType.LAZY)
	private Collection<Contact> contact;
	@OneToMany(mappedBy="employeur",fetch=FetchType.LAZY)
	private Collection<Entreprise> entreprise;
	@OneToMany(mappedBy="employeur",fetch=FetchType.LAZY)
	private Collection<Contrat> contrat;
	@OneToMany(mappedBy="employeur",fetch=FetchType.LAZY)
	private Collection<Consultant> consultant;
	@OneToMany(mappedBy="employeur",fetch=FetchType.LAZY)
	private Collection<CompteRenduActivite> compterenduactivite;
	
	
	public void setUsername(String username) {
		this.username = username;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	@Override
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Date datecreation) {
		this.datecreation = datecreation;
	}
	public String getTypeemp() {
		return typeemp;
	}
	public void setTypeemp(String typeemp) {
		this.typeemp = typeemp;
	}
	public Date getDatemodication() {
		return datemodication;
	}
	public void setDatemodication(Date datemodication) {
		this.datemodication = datemodication;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getService() {
		return service;
	}
	public void setService(String service) {
		this.service = service;
	}
	public Filiale getFiliale() {
		return filiale;
	}
	public void setFiliale(Filiale filiale) {
		this.filiale = filiale;
	}
	/*public Collection<Compte> getComptes() {
		return comptes;
	}*/
	public void setComptes(Collection<Compte> comptes) {
		this.comptes = comptes;
	}
	/*public Collection<Entite> getEntites() {
		return entites;
	}*/
	public void setEntites(Collection<Entite> entites) {
		this.entites = entites;
	}
	/*public Collection<SousEntite> getSousentites() {
		return sousentites;
	}*/
	public void setSousentites(Collection<SousEntite> sousentites) {
		this.sousentites = sousentites;
	}
	/*public Collection<Client> getClients() {
		return clients;
	}*/
	public void setClients(Collection<Client> clients) {
		this.clients = clients;
	}
	/*public Collection<Suivi> getSuivi() {
		return suivi;
	}*/
	public void setSuivi(Collection<Suivi> suivi) {
		this.suivi = suivi;
	}
	
	public void setContact(Collection<Contact> contact) {
		this.contact = contact;
	}
	
	public void setEntreprise(Collection<Entreprise> entreprise) {
		this.entreprise = entreprise;
	}
	
	public void setContrat(Collection<Contrat> contrat) {
		this.contrat = contrat;
	}
	
	public void setConsultant(Collection<Consultant> consultant) {
		this.consultant = consultant;
	}
	
	
	public void setCompterenduactivite(Collection<CompteRenduActivite> compterenduactivite) {
		this.compterenduactivite = compterenduactivite;
	}
	public Employeur() {
		super();
	}
	public Employeur(String username) {
		super();
		this.username = username;
	}
	public Employeur(String username, String nom, String prenom, String password, Date datecreation, String typeemp,
			Date datemodication, String email, String service, Filiale filiale) {
		super();
		this.username = username;
		this.nom = nom;
		this.prenom = prenom;
		this.password = password;
		this.datecreation = datecreation;
		this.typeemp = typeemp;
		this.datemodication = datemodication;
		this.email = email;
		this.service = service;
		this.filiale = filiale;
	}
	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return true;
	}
	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return username;
	}
	
	
	
	
	
	
}
