package org.sid.entities;
import java.util.Collection;
import java.util.Date;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "entites")
public class Entite implements Serializable {
	
	@Id
	@Column(name = "nentite", nullable = false)
		private String nentite;
	@Column(length = 25)
		private String nomentite;
	@Temporal(TemporalType.DATE)
		private Date datecreation;
	@Column(length = 35)
		private String secteur;
		private String adresse;
	@Column(length = 40)
		private String ville;
		private int codepostal;
		private long tel;
		private long tel1;	
		private boolean etat;
		
		@ManyToOne(/*fetch=FetchType.LAZY,cascade=CascadeType.ALL*/)
	    @NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name="codecompter")
		@BatchSize(size=30)
		private Compte compte;
		
		@OneToMany(mappedBy="entite",fetch=FetchType.LAZY)
		private Collection<SousEntite> sousentite;
		
		@ManyToMany
		@JoinTable(name="entiteemployeur")
		private Collection<Employeur> employeur;
		/*@OneToMany(mappedBy="Compte")
		@JoinColumn(name="CodeCompte")
		private Compte compte;*/
		@OneToMany(mappedBy="entite",fetch=FetchType.LAZY)
		private Collection<Offre> offres;

		public String getNentite() {
			return nentite;
		}

		public void setNentite(String nentite) {
			this.nentite = nentite;
		}

		public String getNomentite() {
			return nomentite;
		}

		public void setNomentite(String nomentite) {
			this.nomentite = nomentite;
		}

		public Date getDatecreation() {
			return datecreation;
		}

		public void setDatecreation(Date datecreation) {
			this.datecreation = datecreation;
		}

		public String getSecteur() {
			return secteur;
		}

		public void setSecteur(String secteur) {
			this.secteur = secteur;
		}

		public String getAdresse() {
			return adresse;
		}

		public void setAdresse(String adresse) {
			this.adresse = adresse;
		}

		public String getVille() {
			return ville;
		}

		public void setVille(String ville) {
			this.ville = ville;
		}

		

		public int getCodepostal() {
			return codepostal;
		}

		public void setCodepostal(int codepostal) {
			this.codepostal = codepostal;
		}

		public long getTel() {
			return tel;
		}

		public void setTel(long tel) {
			this.tel = tel;
		}

		public long getTel1() {
			return tel1;
		}

		public void setTel1(long tel1) {
			this.tel1 = tel1;
		}

		public boolean isEtat() {
			return etat;
		}

		public void setEtat(boolean etat) {
			this.etat = etat;
		}

		public Compte getCompte() {
			return compte;
		}
		
		/*public Compte getCompte() {
			//String codecpte;
			return compte;
		}*/

		public void setCompte(Compte compte) {
			this.compte = compte;
		}
		
		/*public Collection<SousEntite> getSousentite() {
			return sousentite;
		}*/

		public void setSousentite(Collection<SousEntite> sousentite) {
			this.sousentite = sousentite;
		}

		/*public Collection<Employeur> getEmployeur() {
			return employeur;
		}*/

		public void setEmployeur(Collection<Employeur> employeur) {
			this.employeur = employeur;
		}
		
		

		public void setOffres(Collection<Offre> offres) {
			this.offres = offres;
		}

		public Entite() {
			super();
		}

		public Entite(String nentite) {
			super();
			this.nentite = nentite;
		}

		public Entite(String nentite, String nomentite, Date datecreation, String secteur, String adresse, String ville,
				int codepostal, long tel, long tel1, boolean etat, Compte compte) {
			super();
			this.nentite = nentite;
			this.nomentite = nomentite;
			this.datecreation = datecreation;
			this.secteur = secteur;
			this.adresse = adresse;
			this.ville = ville;
			this.codepostal = codepostal;
			this.tel = tel;
			this.tel1 = tel1;
			this.etat = etat;
			this.compte = compte;
		}

		

		

		
		
		
		


}
