package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "filiales")
public class Filiale implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer reffililale;
		private String libelle;
		
	@OneToMany(mappedBy="filiale",fetch=FetchType.LAZY)
		private Collection<Employeur> emplyeurs;

	public Integer getReffililale() {
		return reffililale;
	}

	public void setReffililale(Integer reffililale) {
		this.reffililale = reffililale;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	public Collection<Employeur> getEmplyeurs() {
		return emplyeurs;
	}

	public void setEmplyeurs(Collection<Employeur> emplyeurs) {
		this.emplyeurs = emplyeurs;
	}

	public Filiale() {
		super();
	}

	public Filiale(Integer reffililale) {
		super();
		this.reffililale = reffililale;
	}

	public Filiale(Integer reffililale, String libelle) {
		super();
		this.reffililale = reffililale;
		this.libelle = libelle;
	}

		

	
	
}
