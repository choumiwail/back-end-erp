package org.sid.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "commentaires")
public class Commentaire implements Serializable {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer refcommentaire;
		private String description;
	@Temporal(TemporalType.DATE)
		private Date datecommentaire;
	
	@ManyToOne
	@JoinColumn(name="codeacheteurr")
		private Acheteur acheteur;

	public Integer getRefcommentaire() {
		return refcommentaire;
	}

	public void setRefcommentaire(Integer refcommentaire) {
		this.refcommentaire = refcommentaire;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getDatecommentaire() {
		return datecommentaire;
	}

	public void setDatecommentaire(Date datecommentaire) {
		this.datecommentaire = datecommentaire;
	}

	public Acheteur getAcheteur() {
		return acheteur;
	}

	public void setAcheteur(Acheteur acheteur) {
		this.acheteur = acheteur;
	}

	public Commentaire() {
		super();
	}

	public Commentaire(Integer refcommentaire) {
		super();
		this.refcommentaire = refcommentaire;
	}

	public Commentaire(Integer refcommentaire, String description, Date datecommentaire) {
		super();
		this.refcommentaire = refcommentaire;
		this.description = description;
		this.datecommentaire = datecommentaire;
	}

	
	
		

}
