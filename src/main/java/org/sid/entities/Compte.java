package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.BatchSize;





@Entity
@Table(name = "comptes")
public class Compte implements Serializable {
		@Id
		//@GeneratedValue(strategy=GenerationType.AUTO)
		@Column(name = "codecompte", nullable = false)
	private String codecompte;
		@Column(length = 20)
	private String nomcompte;
		@Temporal(TemporalType.DATE)
	private Date datecreation;
	private String adresse;
	private int codepostal;
		@Column(length = 35)
	private String ville;
		@Column(length = 30)
	private String pays;
	private boolean etat;
	private long tel;
	private long fix;
	private String email;
	
	@OneToMany(mappedBy="compte",fetch=FetchType.LAZY/*,cascade=CascadeType.ALL*/)
	@BatchSize(size=30)
	private Collection<Entite> entites;
	@ManyToMany
	@JoinTable(name="compteemployeur")
	private Collection<Employeur> employeur;
	@OneToMany(mappedBy="compte",fetch=FetchType.LAZY)
	private Collection<SousEntite> sousentites;
	@OneToMany(mappedBy="compte",fetch=FetchType.LAZY)
	private Collection<Client> clients;
	@OneToMany(mappedBy="compte",fetch=FetchType.LAZY)
	private Collection<Offre> offres;
	
	
	public String getCodecompte() {
		return codecompte;
	}
	public void setCodecompte(String codecompte) {
		this.codecompte = codecompte;
	}
	public String getNomcompte() {
		return nomcompte;
	}
	public void setNomcompte(String nomcompte) {
		this.nomcompte = nomcompte;
	}
	public Date getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Date datecreation) {
		this.datecreation = datecreation;
	}
	/*public Collection<SousEntite> getSousentites() {
		return sousentites;
	}*/
	public void setSousentites(Collection<SousEntite> sousentites) {
		this.sousentites = sousentites;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	
	public int getCodepostal() {
		return codepostal;
	}
	public void setCodepostal(int codepostal) {
		this.codepostal = codepostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public boolean isEtat() {
		return etat;
	}
	public void setEtat(boolean etat) {
		this.etat = etat;
	}
	public long getTel() {
		return tel;
	}
	public void setTel(long tel) {
		this.tel = tel;
	}
	public long getFix() {
		return fix;
	}
	public void setFix(long fix) {
		this.fix = fix;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	/*public Collection<Entite> getEntites() {
		 
		return entites;
	}*/
	public void setEntites(Collection<Entite> entites) {
		this.entites = entites;
	}
	public Collection<Employeur> getEmployeur() {
		return employeur;
	}
	public void setEmployeur(Collection<Employeur> employeur) {
		this.employeur = employeur;
	}
	
	public void setClients(Collection<Client> clients) {
		this.clients = clients;
	}
	
	
	public void setOffres(Collection<Offre> offres) {
		this.offres = offres;
	}
	public Compte() {
		super();
	}
	public Compte(String codecompte) {
		super();
		this.codecompte = codecompte;
	}
	public Compte(String codecompte, String nomcompte, Date datecreation, String adresse, int codepostal, String ville,
			String pays, boolean etat, long tel, long fix, String email) {
		super();
		this.codecompte = codecompte;
		this.nomcompte = nomcompte;
		this.datecreation = datecreation;
		this.adresse = adresse;
		this.codepostal = codepostal;
		this.ville = ville;
		this.pays = pays;
		this.etat = etat;
		this.tel = tel;
		this.fix = fix;
		this.email = email;
	}
	
	
	
	
		
	
	
}
