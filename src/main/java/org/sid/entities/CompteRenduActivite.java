package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "compterRenduActivites")
public class CompteRenduActivite implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
		private Integer idcompterenduactivite;
		private int jrs;
		private String mois;
		private int annee;
		private String commam;
		private String commpm;
		private float nbrheure;
		private String am;
		private String pm;
		private String mission;
		
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="username")
		private Employeur employeur;

	public Integer getIdcompterenduactivite() {
		return idcompterenduactivite;
	}

	public void setIdcompterenduactivite(Integer idcompterenduactivite) {
		this.idcompterenduactivite = idcompterenduactivite;
	}

	public int getJrs() {
		return jrs;
	}

	public void setJrs(int jrs) {
		this.jrs = jrs;
	}

	public String getMois() {
		return mois;
	}

	public void setMois(String mois) {
		this.mois = mois;
	}

	public int getAnnee() {
		return annee;
	}

	public void setAnnee(int annee) {
		this.annee = annee;
	}

	public String getCommam() {
		return commam;
	}

	public void setCommam(String commam) {
		this.commam = commam;
	}

	public String getCommpm() {
		return commpm;
	}

	public void setCommpm(String commpm) {
		this.commpm = commpm;
	}

	public float getNbrheure() {
		return nbrheure;
	}

	public void setNbrheure(float nbrheure) {
		this.nbrheure = nbrheure;
	}

	

	public String getAm() {
		return am;
	}

	public void setAm(String am) {
		this.am = am;
	}

	public String getPm() {
		return pm;
	}

	public void setPm(String pm) {
		this.pm = pm;
	}

	public String getMission() {
		return mission;
	}

	public void setMission(String mission) {
		this.mission = mission;
	}

	public Employeur getEmployeur() {
		return employeur;
	}

	public void setEmployeur(Employeur employeur) {
		this.employeur = employeur;
	}

	public CompteRenduActivite() {
		super();
	}

	public CompteRenduActivite(int jrs, String mois, int annee, String commam, String commpm, float nbrheure, String am,
			String pm, String mission) {
		super();
		this.jrs = jrs;
		this.mois = mois;
		this.annee = annee;
		this.commam = commam;
		this.commpm = commpm;
		this.nbrheure = nbrheure;
		this.am = am;
		this.pm = pm;
		this.mission = mission;
	}

	public CompteRenduActivite(Integer idcompterenduactivite, int jrs, String mois, int annee, String commam,
			String commpm, float nbrheure, String am, String pm, String mission) {
		super();
		this.idcompterenduactivite = idcompterenduactivite;
		this.jrs = jrs;
		this.mois = mois;
		this.annee = annee;
		this.commam = commam;
		this.commpm = commpm;
		this.nbrheure = nbrheure;
		this.am = am;
		this.pm = pm;
		this.mission = mission;
	}

	

	

	
	
	

}
