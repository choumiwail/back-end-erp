package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "acheteurs")
public class Acheteur implements Serializable {
	
	@Id
	@Column(name = "matriculeacheteur", nullable = false)
		private String matriculeacheteur;
		private String nom;
		private String prenom;
		private String fonction;
		private String adresse;
		private int codepostal;
		private String ville;
		private String pays;
		private String email;
		private String email1;
		private long tel;
		private long tel1;
	@Temporal(TemporalType.DATE)
		private Date datecreation;
 
	/*@ManyToOne
	@JoinColumn(name="codeclientr")
		private Client client;*/
	@OneToMany(mappedBy="acheteur",fetch=FetchType.LAZY)
	private Collection<Client> clients;
	@OneToMany(mappedBy="acheteur",fetch=FetchType.LAZY)
	private Collection<Commentaire> commentaire;
	@OneToMany(mappedBy="acheteur",fetch=FetchType.LAZY)
	private Collection<Offre> offres;
	@OneToMany(mappedBy="acheteur",fetch=FetchType.LAZY)
	private Collection<Contrat> contrats;
	
	public String getMatriculeacheteur() {
		return matriculeacheteur;
	}
	public void setMatriculeacheteur(String matriculeacheteur) {
		this.matriculeacheteur = matriculeacheteur;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getFonction() {
		return fonction;
	}
	public void setFonction(String fonction) {
		this.fonction = fonction;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public int getCodepostal() {
		return codepostal;
	}
	public void setCodepostal(int codepostal) {
		this.codepostal = codepostal;
	}
	public String getVille() {
		return ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getEmail1() {
		return email1;
	}
	public void setEmail1(String email1) {
		this.email1 = email1;
	}
	public long getTel() {
		return tel;
	}
	public void setTel(long tel) {
		this.tel = tel;
	}
	public long getTel1() {
		return tel1;
	}
	public void setTel1(long tel1) {
		this.tel1 = tel1;
	}
	public Date getDatecreation() {
		return datecreation;
	}
	public void setDatecreation(Date datecreation) {
		this.datecreation = datecreation;
	}
	
	/*public Collection<Client> getClients() {
		return clients;
	}*/
	public void setClients(Collection<Client> clients) {
		this.clients = clients;
	}
	/*public Collection<Commentaire> getCommentaire() {
		return commentaire;
	}*/
	public void setCommentaire(Collection<Commentaire> commentaire) {
		this.commentaire = commentaire;
	}
	
	
	
	public void setOffres(Collection<Offre> offres) {
		this.offres = offres;
	}
	
	
	public void setContrat(Collection<Contrat> contrats) {
		this.contrats = contrats;
	}
	public Acheteur() {
		super();
	}
	public Acheteur(String matriculeacheteur) {
		super();
		this.matriculeacheteur = matriculeacheteur;
	}
	public Acheteur(String matriculeacheteur, String nom, String prenom, String fonction, String adresse,
			int codepostal, String ville, String pays, String email, String email1, long tel, long tel1,
			Date datecreation, Collection<Commentaire> commentaire) {
		super();
		this.matriculeacheteur = matriculeacheteur;
		this.nom = nom;
		this.prenom = prenom;
		this.fonction = fonction;
		this.adresse = adresse;
		this.codepostal = codepostal;
		this.ville = ville;
		this.pays = pays;
		this.email = email;
		this.email1 = email1;
		this.tel = tel;
		this.tel1 = tel1;
		this.datecreation = datecreation;
		this.commentaire = commentaire;
	}
	
	
	
	
	
	
}
