package org.sid.entities;

import java.io.Serializable;
import java.util.Date;


import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "contacts")
public class Contact implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Integer numcontact;
	@Temporal(TemporalType.DATE)
	private Date datecontact;
	private boolean besoin;
	private String commentaire;
	
	@ManyToOne
	@JoinColumn(name="codeemployer")
			private Employeur employeur;

	public Integer getNumcontact() {
		return numcontact;
	}

	public void setNumcontact(Integer numcontact) {
		this.numcontact = numcontact;
	}

	public Date getDatecontact() {
		return datecontact;
	}

	public void setDatecontact(Date datecontact) {
		this.datecontact = datecontact;
	}

	

	public boolean isBesoin() {
		return besoin;
	}

	public void setBesoin(boolean besoin) {
		this.besoin = besoin;
	}

	

	public String getCommentaire() {
		return commentaire;
	}

	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}

	public Employeur getEmployeur() {
		return employeur;
	}

	public void setEmployeur(Employeur employeur) {
		this.employeur = employeur;
	}

	public Contact() {
		super();
	}

	public Contact(Date datecontact, boolean besoin, String commentaire) {
		super();
		this.datecontact = datecontact;
		this.besoin = besoin;
		this.commentaire = commentaire;
	}

	public Contact(Integer numcontact, Date datecontact, boolean besoin, String commentaire) {
		super();
		this.numcontact = numcontact;
		this.datecontact = datecontact;
		this.besoin = besoin;
		this.commentaire = commentaire;
	}

	

	
	
	
}
