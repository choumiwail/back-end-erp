package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "suivis")
public class Suivi implements Serializable {
	
	@Id
	@Column(name = "numsuivi", nullable = false)
		private String numsuivi;
	@Temporal(TemporalType.DATE)
		private Date datesuivi;
	    private boolean besoin;
	    private String commentaire;
	@Temporal(TemporalType.DATE)    
	    private Date daterappel;
	    

	@ManyToOne
	@JoinColumn(name="loginr")
			private Employeur employeur;   
	@ManyToOne
	@JoinColumn(name="nrdvr")
			private RendezVous rdv;
	@ManyToOne
	@JoinColumn(name="codeclient")
			private Client client;
	
	public String getNumsuivi() {
		return numsuivi;
	}
	public void setNumsuivi(String numsuivi) {
		this.numsuivi = numsuivi;
	}
	public Date getDatesuivi() {
		return datesuivi;
	}
	public void setDatesuivi(Date datesuivi) {
		this.datesuivi = datesuivi;
	}
	public boolean isBesoin() {
		return besoin;
	}
	public void setBesoin(boolean besoin) {
		this.besoin = besoin;
	}
	public String getCommentaire() {
		return commentaire;
	}
	public void setCommentaire(String commentaire) {
		this.commentaire = commentaire;
	}
	public Employeur getEmployeur() {
		return employeur;
	}
	public void setEmployeur(Employeur employeur) {
		this.employeur = employeur;
	}
	public RendezVous getRdv() {
		return rdv;
	}
	public void setRdv(RendezVous rdv) {
		this.rdv = rdv;
	}
	
	
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	
	
	public Date getDaterappel() {
		return daterappel;
	}
	public void setDaterappel(Date daterappel) {
		this.daterappel = daterappel;
	}
	public Suivi() {
		super();
	}
	public Suivi(String numsuivi) {
		super();
		this.numsuivi = numsuivi;
	}
	public Suivi(String numsuivi, Date datesuivi, boolean besoin, String commentaire, RendezVous rdv) {
		super();
		this.numsuivi = numsuivi;
		this.datesuivi = datesuivi;
		this.besoin = besoin;
		this.commentaire = commentaire;
		this.rdv = rdv;
	}
	
	
	
}
