package org.sid.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "consultants")
public class Consultant implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer nconsultant;
	private String nom;
	private String prenom;
	private String grade;
	@Temporal(TemporalType.DATE)
	private Date datelinks;
	private boolean periode;
	@Temporal(TemporalType.DATE)
	private Date dateperiode;
	private float salairebrut;
	private float salairenet;
	
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="login")
		private Employeur employeur;

	public Integer getNconsultant() {
		return nconsultant;
	}

	public void setNconsultant(Integer nconsultant) {
		this.nconsultant = nconsultant;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getGrade() {
		return grade;
	}

	public void setGrade(String grade) {
		this.grade = grade;
	}

	public Date getDatelinks() {
		return datelinks;
	}

	public void setDatelinks(Date datelinks) {
		this.datelinks = datelinks;
	}

	public boolean isPeriode() {
		return periode;
	}

	public void setPeriode(boolean periode) {
		this.periode = periode;
	}

	public Date getDateperiode() {
		return dateperiode;
	}

	public void setDateperiode(Date dateperiode) {
		this.dateperiode = dateperiode;
	}

	public float getSalairebrut() {
		return salairebrut;
	}

	public void setSalairebrut(float salairebrut) {
		this.salairebrut = salairebrut;
	}

	public float getSalairenet() {
		return salairenet;
	}

	public void setSalairenet(float salairenet) {
		this.salairenet = salairenet;
	}

	public Employeur getEmployeur() {
		return employeur;
	}

	public void setEmployeur(Employeur employeur) {
		this.employeur = employeur;
	}

	public Consultant() {
		super();
	}

	public Consultant(String nom, String prenom, String grade, Date datelinks, boolean periode, Date dateperiode,
			float salairebrut, float salairenet) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.grade = grade;
		this.datelinks = datelinks;
		this.periode = periode;
		this.dateperiode = dateperiode;
		this.salairebrut = salairebrut;
		this.salairenet = salairenet;
	}

	public Consultant(Integer nconsultant, String nom, String prenom, String grade, Date datelinks, boolean periode,
			Date dateperiode, float salairebrut, float salairenet) {
		super();
		this.nconsultant = nconsultant;
		this.nom = nom;
		this.prenom = prenom;
		this.grade = grade;
		this.datelinks = datelinks;
		this.periode = periode;
		this.dateperiode = dateperiode;
		this.salairebrut = salairebrut;
		this.salairenet = salairenet;
	}
	
	
}
