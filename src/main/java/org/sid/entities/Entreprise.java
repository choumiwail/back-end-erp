package org.sid.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "entreprises")
public class Entreprise implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer identreprise;
	private String nomentreprise;
	private long tel;
	private long tel1;
	private String email;
	private String adresse;
	private String rcs;
	private String codeape;
	private String tva;
	private String nsiret;
	private float penalite;
	private float indemnite;
	private String nombanque;
	private String rib;
	private String iban;
	private String bic;
	
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="codemployeur")
		private Employeur employeur;

	public Integer getIdentreprise() {
		return identreprise;
	}

	public void setIdentreprise(Integer identreprise) {
		this.identreprise = identreprise;
	}

	public String getNomentreprise() {
		return nomentreprise;
	}

	public void setNomentreprise(String nomentreprise) {
		this.nomentreprise = nomentreprise;
	}

	public long getTel() {
		return tel;
	}

	public void setTel(long tel) {
		this.tel = tel;
	}

	public long getTel1() {
		return tel1;
	}

	public void setTel1(long tel1) {
		this.tel1 = tel1;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}


	public String getRcs() {
		return rcs;
	}

	public void setRcs(String rcs) {
		this.rcs = rcs;
	}

	public String getCodeape() {
		return codeape;
	}

	public void setCodeape(String codeape) {
		this.codeape = codeape;
	}

	public String getTva() {
		return tva;
	}

	public void setTva(String tva) {
		this.tva = tva;
	}

	public String getNsiret() {
		return nsiret;
	}

	public void setNsiret(String nsiret) {
		this.nsiret = nsiret;
	}

	public float getPenalite() {
		return penalite;
	}

	public void setPenalite(float penalite) {
		this.penalite = penalite;
	}

	public float getIndemnite() {
		return indemnite;
	}

	public void setIndemnite(float indemnite) {
		this.indemnite = indemnite;
	}

	public String getNombanque() {
		return nombanque;
	}

	public void setNombanque(String nombanque) {
		this.nombanque = nombanque;
	}

	public String getRib() {
		return rib;
	}

	public void setRib(String rib) {
		this.rib = rib;
	}

	public String getIban() {
		return iban;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public String getBic() {
		return bic;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public Employeur getEmployeur() {
		return employeur;
	}

	public void setEmployeur(Employeur employeur) {
		this.employeur = employeur;
	}

	public Entreprise() {
		super();
	}

	public Entreprise(String nomentreprise, long tel, long tel1, String email, String adresse, String rcs,
			String codeape, String tva, String nsiret, float penalite, float indemnite, String nombanque, String rib,
			String iban, String bic) {
		super();
		this.nomentreprise = nomentreprise;
		this.tel = tel;
		this.tel1 = tel1;
		this.email = email;
		this.adresse = adresse;
		this.rcs = rcs;
		this.codeape = codeape;
		this.tva = tva;
		this.nsiret = nsiret;
		this.penalite = penalite;
		this.indemnite = indemnite;
		this.nombanque = nombanque;
		this.rib = rib;
		this.iban = iban;
		this.bic = bic;
	}

	public Entreprise(Integer identreprise, String nomentreprise, long tel, long tel1, String email, String adresse,
			String rcs, String codeape, String tva, String nsiret, float penalite, float indemnite, String nombanque,
			String rib, String iban, String bic) {
		super();
		this.identreprise = identreprise;
		this.nomentreprise = nomentreprise;
		this.tel = tel;
		this.tel1 = tel1;
		this.email = email;
		this.adresse = adresse;
		this.rcs = rcs;
		this.codeape = codeape;
		this.tva = tva;
		this.nsiret = nsiret;
		this.penalite = penalite;
		this.indemnite = indemnite;
		this.nombanque = nombanque;
		this.rib = rib;
		this.iban = iban;
		this.bic = bic;
	}

	
	
	

}
