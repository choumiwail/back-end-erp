package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "offres")
public class Offre implements Serializable{
	
	@Id
	@Column(name = "refappel", nullable = false)
		private String refappel;
		private String intitule;
	@Temporal(TemporalType.DATE)
		private Date dateemission;
	@Temporal(TemporalType.DATE)
		private Date dateexpriration;
	@Temporal(TemporalType.DATE)
		private Date datedemarrage;
	@Temporal(TemporalType.DATE)
		private Date datecloture;
		private String lieu;
		private int nbrtravaill;
		private String sujet;
		private String profil;
		private String experience;
		private String competencecle;
		private String competencefon;
		private String competencetech;
		private String objectif;
		private String prestation;
		private String autreinfo;
		private String langue1;
		private String langue2;
		private String langue3;
	
		@ManyToOne
		@NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name="codeclient")
			private Client client;
		
		@ManyToOne
		@NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name="codeacheteur")
			private Acheteur acheteur;
		
		@ManyToOne
		@NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name="codecompte")
			private Compte compte;
		
		@ManyToOne
		@NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name="nentite")
			private Entite entite;
		
		@ManyToOne
		@NotFound(action = NotFoundAction.IGNORE)
		@JoinColumn(name="nsentite")
			private SousEntite sousentite;
		
		@OneToMany(mappedBy="offre",fetch=FetchType.LAZY)
		private Collection<Contrat> contrats;

		public String getRefappel() {
			return refappel;
		}

		public void setRefappel(String refappel) {
			this.refappel = refappel;
		}

		public String getIntitule() {
			return intitule;
		}

		public void setIntitule(String intitule) {
			this.intitule = intitule;
		}

		public Date getDateemission() {
			return dateemission;
		}

		public void setDateemission(Date dateemission) {
			this.dateemission = dateemission;
		}

		public Date getDateexpriration() {
			return dateexpriration;
		}

		public void setDateexpriration(Date dateexpriration) {
			this.dateexpriration = dateexpriration;
		}

		public Date getDatedemarrage() {
			return datedemarrage;
		}

		public void setDatedemarrage(Date datedemarrage) {
			this.datedemarrage = datedemarrage;
		}

		public Date getDatecloture() {
			return datecloture;
		}

		public void setDatecloture(Date datecloture) {
			this.datecloture = datecloture;
		}

		public String getLieu() {
			return lieu;
		}

		public void setLieu(String lieu) {
			this.lieu = lieu;
		}

		public int getNbrtravaill() {
			return nbrtravaill;
		}

		public void setNbrtravaill(int nbrtravaill) {
			this.nbrtravaill = nbrtravaill;
		}

		public String getSujet() {
			return sujet;
		}

		public void setSujet(String sujet) {
			this.sujet = sujet;
		}

		public String getProfil() {
			return profil;
		}

		public void setProfil(String profil) {
			this.profil = profil;
		}

		public String getObjectif() {
			return objectif;
		}

		public void setObjectif(String objectif) {
			this.objectif = objectif;
		}

		public String getPrestation() {
			return prestation;
		}

		public void setPrestation(String prestation) {
			this.prestation = prestation;
		}

		public String getAutreinfo() {
			return autreinfo;
		}

		public void setAutreinfo(String autreinfo) {
			this.autreinfo = autreinfo;
		}

		public String getLangue1() {
			return langue1;
		}

		public void setLangue1(String langue1) {
			this.langue1 = langue1;
		}

		public String getLangue2() {
			return langue2;
		}

		public void setLangue2(String langue2) {
			this.langue2 = langue2;
		}

		public String getLangue3() {
			return langue3;
		}

		public void setLangue3(String langue3) {
			this.langue3 = langue3;
		}

		public Client getClient() {
			return client;
		}

		public void setClient(Client client) {
			this.client = client;
		}

		public Acheteur getAcheteur() {
			return acheteur;
		}

		public void setAcheteur(Acheteur acheteur) {
			this.acheteur = acheteur;
		}

		public Compte getCompte() {
			return compte;
		}

		public void setCompte(Compte compte) {
			this.compte = compte;
		}

		public Entite getEntite() {
			return entite;
		}

		public void setEntite(Entite entite) {
			this.entite = entite;
		}

		public SousEntite getSousentite() {
			return sousentite;
		}

		public void setSousentite(SousEntite sousentite) {
			this.sousentite = sousentite;
		}
		
		

		public String getExperience() {
			return experience;
		}

		public void setExperience(String experience) {
			this.experience = experience;
		}

		public String getCompetencecle() {
			return competencecle;
		}

		public void setCompetencecle(String competencecle) {
			this.competencecle = competencecle;
		}

		public String getCompetencefon() {
			return competencefon;
		}

		public void setCompetencefon(String competencefon) {
			this.competencefon = competencefon;
		}

		public String getCompetencetech() {
			return competencetech;
		}

		public void setCompetencetech(String competencetech) {
			this.competencetech = competencetech;
		}
		
		

		public void setContrat(Collection<Contrat> contrats) {
			this.contrats = contrats;
		}

		public Offre() {
			super();
		}

		public Offre(String intitule, Date dateemission, Date dateexpriration, Date datedemarrage, Date datecloture,
				String lieu, int nbrtravaill, String sujet, String profil, String experience, String competencecle,
				String competencefon, String competencetech, String objectif, String prestation, String autreinfo,
				String langue1, String langue2, String langue3) {
			super();
			this.intitule = intitule;
			this.dateemission = dateemission;
			this.dateexpriration = dateexpriration;
			this.datedemarrage = datedemarrage;
			this.datecloture = datecloture;
			this.lieu = lieu;
			this.nbrtravaill = nbrtravaill;
			this.sujet = sujet;
			this.profil = profil;
			this.experience = experience;
			this.competencecle = competencecle;
			this.competencefon = competencefon;
			this.competencetech = competencetech;
			this.objectif = objectif;
			this.prestation = prestation;
			this.autreinfo = autreinfo;
			this.langue1 = langue1;
			this.langue2 = langue2;
			this.langue3 = langue3;
		}

		public Offre(String refappel, String intitule, Date dateemission, Date dateexpriration, Date datedemarrage,
				Date datecloture, String lieu, int nbrtravaill, String sujet, String profil, String experience,
				String competencecle, String competencefon, String competencetech, String objectif, String prestation,
				String autreinfo, String langue1, String langue2, String langue3) {
			super();
			this.refappel = refappel;
			this.intitule = intitule;
			this.dateemission = dateemission;
			this.dateexpriration = dateexpriration;
			this.datedemarrage = datedemarrage;
			this.datecloture = datecloture;
			this.lieu = lieu;
			this.nbrtravaill = nbrtravaill;
			this.sujet = sujet;
			this.profil = profil;
			this.experience = experience;
			this.competencecle = competencecle;
			this.competencefon = competencefon;
			this.competencetech = competencetech;
			this.objectif = objectif;
			this.prestation = prestation;
			this.autreinfo = autreinfo;
			this.langue1 = langue1;
			this.langue2 = langue2;
			this.langue3 = langue3;
		}

		public Offre(String refappel) {
			super();
			this.refappel = refappel;
		}
		
		

		
		
}
