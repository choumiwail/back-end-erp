package org.sid.entities;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "contrats")
public class Contrat implements Serializable{

	@Id
	@Column(name = "ncontrat", nullable = false)
	private String ncontrat;
	private String natureprestation;
	private String adresse;
	private String ville;
	private float prixjrht;
	private String frefacture;
	@Temporal(TemporalType.DATE)
	private Date datecloture;
	
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="codemployeur")
		private Employeur employeur;
	
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="codeclient")
		private Client client;
	
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="codacheteur")
		private Acheteur acheteur;
	
	@ManyToOne/*(cascade = {CascadeType.DETACH, CascadeType.MERGE, CascadeType.REFRESH})*/
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="refappel")
		private Offre offre;
	
	@OneToMany(mappedBy="contrat",fetch=FetchType.LAZY)
	private Collection<Facture> factures;

	public String getNcontrat() {
		return ncontrat;
	}

	public void setNcontrat(String ncontrat) {
		this.ncontrat = ncontrat;
	}

	public String getNatureprestation() {
		return natureprestation;
	}

	public void setNatureprestation(String natureprestation) {
		this.natureprestation = natureprestation;
	}

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getVille() {
		return ville;
	}

	public void setVille(String ville) {
		this.ville = ville;
	}

	public float getPrixjrht() {
		return prixjrht;
	}

	public void setPrixjrht(float prixjrht) {
		this.prixjrht = prixjrht;
	}

	public String getFrefacture() {
		return frefacture;
	}

	public void setFrefacture(String frefacture) {
		this.frefacture = frefacture;
	}

	public Date getDatecloture() {
		return datecloture;
	}

	public void setDatecloture(Date datecloture) {
		this.datecloture = datecloture;
	}

	public Employeur getEmployeur() {
		return employeur;
	}

	public void setEmployeur(Employeur employeur) {
		this.employeur = employeur;
	}

	public Client getClient() {
		return client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public Acheteur getAcheteur() {
		return acheteur;
	}

	public void setAcheteur(Acheteur acheteur) {
		this.acheteur = acheteur;
	}

	public Offre getOffre() {
		return offre;
	}

	public void setOffre(Offre offre) {
		this.offre = offre;
	}
	
	

	public void setFactures(Collection<Facture> factures) {
		this.factures = factures;
	}

	public Contrat() {
		super();
	}

	public Contrat(String natureprestation, String adresse, String ville, float prixjrht, String frefacture,
			Date datecloture) {
		super();
		this.natureprestation = natureprestation;
		this.adresse = adresse;
		this.ville = ville;
		this.prixjrht = prixjrht;
		this.frefacture = frefacture;
		this.datecloture = datecloture;
	}

	public Contrat(String ncontrat, String natureprestation, String adresse, String ville, float prixjrht,
			String frefacture, Date datecloture) {
		super();
		this.ncontrat = ncontrat;
		this.natureprestation = natureprestation;
		this.adresse = adresse;
		this.ville = ville;
		this.prixjrht = prixjrht;
		this.frefacture = frefacture;
		this.datecloture = datecloture;
	}
	
	
	
	
	
}
