package org.sid.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

@Entity
@Table(name = "factures")
public class Facture implements Serializable{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer nfacture;
	@Temporal(TemporalType.DATE)
	private Date datefacture;
	private String moisannes;
	private int nbrtravaille;
	private float montantht;
	private float tva;
	private float netpaye;
	
	@ManyToOne
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name="ncontrat")
		private Contrat contrat;

	public Integer getNfacture() {
		return nfacture;
	}

	public void setNfacture(Integer nfacture) {
		this.nfacture = nfacture;
	}

	public Date getDatefacture() {
		return datefacture;
	}

	public void setDatefacture(Date datefacture) {
		this.datefacture = datefacture;
	}

	public int getNbrtravaille() {
		return nbrtravaille;
	}

	public void setNbrtravaille(int nbrtravaille) {
		this.nbrtravaille = nbrtravaille;
	}

	public float getMontantht() {
		return montantht;
	}

	public void setMontantht(float montantht) {
		this.montantht = montantht;
	}

	public float getTva() {
		return tva;
	}

	public void setTva(float tva) {
		this.tva = tva;
	}

	public float getNetpaye() {
		return netpaye;
	}

	public void setNetpaye(float netpaye) {
		this.netpaye = netpaye;
	}

	public Contrat getContrat() {
		return contrat;
	}

	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}
	

	public String getMoisannes() {
		return moisannes;
	}

	public void setMoisannes(String moisannes) {
		this.moisannes = moisannes;
	}

	public Facture() {
		super();
	}

	public Facture(Date datefacture, String moisannes, int nbrtravaille, float montantht, float tva, float netpaye) {
		super();
		this.datefacture = datefacture;
		this.moisannes = moisannes;
		this.nbrtravaille = nbrtravaille;
		this.montantht = montantht;
		this.tva = tva;
		this.netpaye = netpaye;
	}

	public Facture(Integer nfacture, Date datefacture, String moisannes, int nbrtravaille, float montantht, float tva,
			float netpaye) {
		super();
		this.nfacture = nfacture;
		this.datefacture = datefacture;
		this.moisannes = moisannes;
		this.nbrtravaille = nbrtravaille;
		this.montantht = montantht;
		this.tva = tva;
		this.netpaye = netpaye;
	}

	
	
	
}
