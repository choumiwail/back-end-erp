package org.sid.dao;

import java.util.List;

import org.sid.entities.Compte;
import org.sid.entities.Suivi;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface SuiviRepository extends JpaRepository<Suivi, String> {
	
	@Query("select c.codecompte,c.nomcompte from Compte as c,Client as cl "
			+ "where c.codecompte=cl.compte "
			+ "and cl.matriculeclient like :x ")
	public List<Object> getListOfCompte(@Param("x") String mc);
	
	@Query("select su.numsuivi,su.datesuivi,cl.matriculeclient,cl.nom,cl.prenom,cl.fonction,c.nomcompte,e.nomentite,se.nomsentite,CONCAT(ach.nom, ' ', ach.prenom),"
			+ "cl.etat, cl.secteur, cl.direction, cl.adresse, cl.codepostal, cl.ville, cl.pays, cl.hierarchie, cl.hierarchis, cl.tel, cl.tel1, cl.portableperso, "
			+ "cl.portablepro, cl.ficheclient, cl.activiteclient, cl.email, cl.email1 "
			+ "from Compte as c,Client as cl, Suivi as su, Entite as e, SousEntite as se , Acheteur as ach  "
			+ "where su.client=cl.matriculeclient and c.codecompte=cl.compte and c.codecompte=e.compte and c.codecompte=se.compte and e.nentite=se.entite and cl.acheteur = ach.matriculeacheteur ")
	public List<Object> getListOfSuivi();
	
	/*findByEntite*/
	@Query("select su.numsuivi,su.datesuivi,cl.matriculeclient,cl.nom,cl.prenom,cl.fonction,c.nomcompte,e.nomentite,se.nomsentite,CONCAT(ach.nom, ' ', ach.prenom),"
			+ "cl.etat, cl.secteur, cl.direction, cl.adresse, cl.codepostal, cl.ville, cl.pays, cl.hierarchie, cl.hierarchis, cl.tel, cl.tel1, cl.portableperso, "
			+ "cl.portablepro, cl.ficheclient, cl.activiteclient, cl.email, cl.email1 "
			+ "from Compte as c,Client as cl, Suivi as su, Entite as e, SousEntite as se , Acheteur as ach  "
			+ "where su.client=cl.matriculeclient and c.codecompte=cl.compte and c.codecompte=e.compte and c.codecompte=se.compte and e.nentite=se.entite and cl.acheteur = ach.matriculeacheteur "
			+ "and e.nomentite like :mc ")
	public Page<Object> findByEntite(@Param("mc") String mc, Pageable pageable);
	
	/*findBySEntite*/
	@Query("select su.numsuivi,su.datesuivi,cl.matriculeclient,cl.nom,cl.prenom,cl.fonction,c.nomcompte,e.nomentite,se.nomsentite,CONCAT(ach.nom, ' ', ach.prenom),"
			+ "cl.etat, cl.secteur, cl.direction, cl.adresse, cl.codepostal, cl.ville, cl.pays, cl.hierarchie, cl.hierarchis, cl.tel, cl.tel1, cl.portableperso, "
			+ "cl.portablepro, cl.ficheclient, cl.activiteclient, cl.email, cl.email1 "
			+ "from Compte as c,Client as cl, Suivi as su, Entite as e, SousEntite as se , Acheteur as ach  "
			+ "where su.client=cl.matriculeclient and c.codecompte=cl.compte and c.codecompte=e.compte and c.codecompte=se.compte and e.nentite=se.entite and cl.acheteur = ach.matriculeacheteur "
			+ "and se.nomsentite like :mc ")
	public Page<Object> findBySEntite(@Param("mc") String mc, Pageable pageable);
	
	/*findByClient*/
	@Query("select su.numsuivi,su.datesuivi,cl.matriculeclient,cl.nom,cl.prenom,cl.fonction,c.nomcompte,e.nomentite,se.nomsentite,CONCAT(ach.nom, ' ', ach.prenom),"
			+ "cl.etat, cl.secteur, cl.direction, cl.adresse, cl.codepostal, cl.ville, cl.pays, cl.hierarchie, cl.hierarchis, cl.tel, cl.tel1, cl.portableperso, "
			+ "cl.portablepro, cl.ficheclient, cl.activiteclient, cl.email, cl.email1 "
			+ "from Compte as c,Client as cl, Suivi as su, Entite as e, SousEntite as se , Acheteur as ach  "
			+ "where su.client=cl.matriculeclient and c.codecompte=cl.compte and c.codecompte=e.compte and c.codecompte=se.compte and e.nentite=se.entite and cl.acheteur = ach.matriculeacheteur "
			+ "and cl.nom like :mc ")
	public Page<Object> findByClient(@Param("mc") String mc, Pageable pageable);
	
	/*findByCompte*/
	@Query("select su.numsuivi,su.datesuivi,cl.matriculeclient,cl.nom,cl.prenom,cl.fonction,c.nomcompte,e.nomentite,se.nomsentite,CONCAT(ach.nom, ' ', ach.prenom),"
			+ "cl.etat, cl.secteur, cl.direction, cl.adresse, cl.codepostal, cl.ville, cl.pays, cl.hierarchie, cl.hierarchis, cl.tel, cl.tel1, cl.portableperso, "
			+ "cl.portablepro, cl.ficheclient, cl.activiteclient, cl.email, cl.email1 "
			+ "from Compte as c,Client as cl, Suivi as su, Entite as e, SousEntite as se , Acheteur as ach  "
			+ "where su.client=cl.matriculeclient and c.codecompte=cl.compte and c.codecompte=e.compte and c.codecompte=se.compte and e.nentite=se.entite and cl.acheteur = ach.matriculeacheteur "
			+ "and c.nomcompte like :mc ")
	public Page<Object> findByCompte(@Param("mc") String mc, Pageable pageable);
	
 
	/*select cl.matriculeclient,cl.nom,cl.prenom,cl.direction,cl.fonction,cl.hierarchie,cl.hierarchis,
	cl.adresse,cl.tel,cl.tel1,cl.email,c.nomcompte,e.nomentite,se.nomsentite
	from clients cl, comptes c, entites e, sousentites se
	where c.codecompte=cl.codecompte and c.codecompte=e.codecompter and c.codecompte=se.codecompter and e.nentite=se.nentiter
	and cl.matriculeclient like 'CL01';  and e.nentite like :z
	
	,cl.nom,cl.prenom,cl.direction,cl.fonction,cl.adresse,cl.hierarchie,"
			+ "cl.hierarchis,cl.tel,cl.tel1,cl.email
	*/
	
	
}
