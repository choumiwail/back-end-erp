package org.sid.dao;

import java.util.List;

import org.sid.entities.SousEntite;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SousEntiteRepository extends JpaRepository<SousEntite, String> {
	
	@Query("select e.nentite,e.nomentite from Compte as c, Entite as e "
			+ "where c.codecompte=e.compte "
			+ "and c.codecompte like :x")
	public List<Object> getListOfEntite(@Param("x") String mc);

}
