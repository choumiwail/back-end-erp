package org.sid.dao;

import org.sid.entities.CompteRenduActivite;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CompteRenduActiviteRepository extends JpaRepository<CompteRenduActivite , Integer>{

}
