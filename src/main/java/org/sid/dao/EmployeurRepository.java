package org.sid.dao;

import org.sid.entities.Employeur;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeurRepository extends JpaRepository<Employeur, String>  {
	
	public Employeur findOneByUsername(String username);

}
