package org.sid.dao;

import java.util.List;

import org.sid.entities.Client;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ClientRepository extends JpaRepository<Client, String> {

	@Query("select se.nsentite,se.nomsentite from  Entite as e, SousEntite as se "
			+ "where e.nentite=se.entite "
			+ "and e.nentite like :x")
	public List<Object> getListOfSE(@Param("x") String mc);
	
	@Query("select c.codecompte,c.nomcompte from Compte as c, Client as cl "
			+ "where c.codecompte=cl.compte "
			+ "and cl.matriculeclient like :x")
	public List<Client> getListOfCompte(@Param("x") String mc);
}
