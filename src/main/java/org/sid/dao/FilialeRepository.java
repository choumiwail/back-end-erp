package org.sid.dao;

import org.sid.entities.Filiale;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FilialeRepository extends JpaRepository<Filiale, Integer>  {

}
