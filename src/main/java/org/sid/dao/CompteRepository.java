package org.sid.dao;



import java.util.List;

import org.sid.entities.Client;
import org.sid.entities.Compte;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface CompteRepository extends JpaRepository<Compte, String> {

	@Query("select c from Compte c where c.nomcompte like :mc")
	public Page<Compte> chercher(@Param("mc") String mc, Pageable pageable);
	
	
	
	/*@Query("select max(c.codecompte) as codecompte from Compte c ")
	public Object getValOfCpt();*/
}
