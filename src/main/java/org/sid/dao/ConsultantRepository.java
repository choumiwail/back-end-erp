package org.sid.dao;

import org.sid.entities.Consultant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsultantRepository extends JpaRepository<Consultant, Integer> {

}
