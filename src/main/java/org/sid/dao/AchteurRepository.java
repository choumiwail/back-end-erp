package org.sid.dao;

import java.util.List;

import org.sid.entities.Acheteur;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface AchteurRepository extends JpaRepository<Acheteur, String> {

	@Query("select ach.matriculeacheteur, ach.adresse, ach.codepostal, ach.datecreation, ach.email, ach.email1, ach.fonction, ach.nom, ach.pays, ach.prenom,"
			+ "ach.tel, ach.tel1, ach.ville, com.datecommentaire, com.description From Commentaire as com, Acheteur as ach "
			+ "where ach.matriculeacheteur=com.acheteur "
			+ "and ach.matriculeacheteur like :x")
	public List<Object> getListOfAcheteurs(@Param("x") String mc);

}
