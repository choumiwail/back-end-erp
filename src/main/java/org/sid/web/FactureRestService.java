package org.sid.web;


import java.util.List;


import org.sid.dao.FactureRepository;
import org.sid.entities.Facture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/factures")
public class FactureRestService {
	
	@Autowired
	private FactureRepository factureRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Facture> getFactures(){
		return factureRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{nfacture}", method=RequestMethod.GET)
	public Facture getFacture(@PathVariable Integer nfacture){
		return factureRepository.findOne(nfacture);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Facture save(@RequestBody Facture facture) {
		
		return factureRepository.save(facture);
	}
	
	/*Update*/
	@RequestMapping(value="/{nfacture}", method=RequestMethod.PUT)
	public Facture save(@PathVariable Integer nfacture,@RequestBody Facture facture) {
		facture.setNfacture(nfacture);
		return factureRepository.save(facture);
	}

}

