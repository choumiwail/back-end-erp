package org.sid.web;

import org.sid.dao.CompteRenduActiviteRepository;
import org.sid.entities.CompteRenduActivite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/compterRenduActivites")
public class CompteRdAcRestService {


	@Autowired
	private CompteRenduActiviteRepository compteRenduActiviteRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<CompteRenduActivite> getCras(){
		return compteRenduActiviteRepository.findAll();
	}
	
	/*GetbyID*/
	@RequestMapping(value="/{idCra}", method=RequestMethod.GET)
	public CompteRenduActivite getCra(@PathVariable Integer idCra){
		return compteRenduActiviteRepository.findOne(idCra);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public CompteRenduActivite save(@RequestBody List<CompteRenduActivite> cra){
		/*return (CompteRenduActivite) compteRenduActiviteRepository.save(cra.stream().collect(Collectors.toList()));*/
		compteRenduActiviteRepository.flush();
		CompteRenduActivite compte = null;
		for(CompteRenduActivite compteRenduActivite: cra ) {
			 compte= compteRenduActiviteRepository.save(compteRenduActivite);
		}
		return compte;
	}
}
