package org.sid.web;

import java.util.List;

import org.sid.dao.OffreRepository;
import org.sid.entities.Offre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/offres")

public class OffreRestService {
	
	@Autowired
	private OffreRepository offreRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Offre> getOffres(){
		return offreRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{refappel}", method=RequestMethod.GET)
	public Offre getOffre(@PathVariable String refappel){
		return offreRepository.findOne(refappel);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Offre save(@RequestBody Offre refappel) {
		
		return offreRepository.save(refappel);
	}
	
	/*Update*/
	@RequestMapping(value="/{refappel}", method=RequestMethod.PUT)
	public Offre save(@PathVariable String refappel,@RequestBody Offre offre) {
		offre.setRefappel(refappel);
		return offreRepository.save(offre);
	}

}
