package org.sid.web;

import java.security.Principal;
import java.util.List;

import org.sid.dao.EmployeurRepository;
import org.sid.entities.Employeur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@RestController
@CrossOrigin("*")
@RequestMapping(value="/employeurs")
public class EmployeurRestService {
	
	public static final Logger logger = LoggerFactory.getLogger(EmployeurRestService.class);
	
	@Autowired
	private EmployeurRepository employeurRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Employeur> getEmployeurs(){
		return employeurRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{login}", method=RequestMethod.GET)
	public Employeur getEmployeur(@PathVariable String login){
		return employeurRepository.findOne(login);
	}
	
	/*GetById*/
	public Employeur getEmployeu(String username){
		return employeurRepository.findOne(username);
	}
	
	
	
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Employeur save(@RequestBody Employeur employeur) {
		
		return employeurRepository.save(employeur);
	}
	
	/*Update*/
	@RequestMapping(value="/{login}", method=RequestMethod.PUT)
	public Employeur save(@PathVariable String username,@RequestBody Employeur employeur) {
		employeur.setUsername(username);
		return employeurRepository.save(employeur);
	}
	
	// this is the login api/service
		@CrossOrigin
		@RequestMapping("/login")
		public Principal user(Principal principal) {
			logger.info("user logged "+principal);
			return principal;
		}


}
