package org.sid.web;


import java.util.List;

import org.sid.dao.CompteRepository;
import org.sid.entities.Client;
import org.sid.entities.Compte;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/*spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect*/

@RestController
/*Autorist to the navigator to accees query/ data */
@CrossOrigin("*")
@RequestMapping(value="/comptes")
public class CompteRestService {
	@Autowired
	private CompteRepository compteRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Compte> getComptes(){
		return compteRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{codecompte}", method=RequestMethod.GET)
	public Compte getCompte(@PathVariable String codecompte){
		return compteRepository.findOne(codecompte);
	}
	/*GetValueOfCompte*/
	/*@RequestMapping(value="/getValOfCpt", method=RequestMethod.GET)
	public Object getValOfCpt(){
		return compteRepository.getValOfCpt();
	}*/
	
	/*Search*/
	@RequestMapping(value="/ChercherComptes", method=RequestMethod.GET)
	public Page<Compte> chercherCom(
			@RequestParam(name="mc", defaultValue="") String mc,
			@RequestParam(name="page", defaultValue="0") int page,
			@RequestParam(name="size", defaultValue="5") int size){
		return compteRepository.chercher("%"+mc+"%", new PageRequest(page, size));
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Compte save(@RequestBody Compte compte){
		return compteRepository.save(compte);
	}
	
	/*Delete*/
	@RequestMapping(value="/{codecompte}", method=RequestMethod.DELETE)
	public boolean supprimer(@PathVariable String codecompte){
		compteRepository.delete(codecompte);
		return true;
	}
	
	/*Update*/
	@RequestMapping(value="/{codecompte}", method=RequestMethod.PUT)
	public Compte save(@PathVariable String codecompte,@RequestBody Compte compte){
		compte.setCodecompte(codecompte);
		return compteRepository.save(compte);
	}
	
	
}
