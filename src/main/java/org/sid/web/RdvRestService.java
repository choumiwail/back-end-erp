package org.sid.web;

import java.util.List;

import org.sid.dao.RendezVousRepository;
import org.sid.entities.RendezVous;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/rendezvous")
public class RdvRestService {
	
	@Autowired
	private RendezVousRepository rendezVousRepository; 
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<RendezVous> getRdvs(){
		return rendezVousRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{nrdv}", method=RequestMethod.GET)
	public RendezVous getRdv(@PathVariable String nrdv){
		return rendezVousRepository.findOne(nrdv);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public RendezVous save(@RequestBody RendezVous nrdv) {
		return rendezVousRepository.save(nrdv);
	}

}
