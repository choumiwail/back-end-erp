package org.sid.web;

import java.util.List;

import org.sid.dao.ClientRepository;
import org.sid.entities.Acheteur;
import org.sid.entities.Client;
import org.sid.entities.SousEntite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/clients")

public class ClientRestService {
	
	@Autowired
	private ClientRepository clientRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Client> getClients(){
		return clientRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{nclient}", method=RequestMethod.GET)
	public Client getClient(@PathVariable String nclient){
		return clientRepository.findOne(nclient);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Client save(@RequestBody Client nclient) {
		
		return clientRepository.save(nclient);
	}
	
	/*Update*/
	@RequestMapping(value="/{nclient}", method=RequestMethod.PUT)
	public Client save(@PathVariable String nclient,@RequestBody Client client) {
		client.setMatriculeclient(nclient);
		return clientRepository.save(client);
	}
	
	/*Get all entite & sous-entite by code compte*/
	
	@RequestMapping(value="/getEntiteSous", method=RequestMethod.GET)
	public List<Object> getListOfSEMc(@RequestParam(name="mc", defaultValue="") String mc){
		System.out.println(mc);
		return clientRepository.getListOfSE(mc);
		
	}
	
	/*ListofCompte by Client*/
	@RequestMapping(value="/getCompte", method=RequestMethod.GET)
	public List<Client> getListOfCompteMc(@RequestParam(name="mc", defaultValue="") String mc){
		System.out.println(mc);
		return clientRepository.getListOfCompte(mc);
	}

}
