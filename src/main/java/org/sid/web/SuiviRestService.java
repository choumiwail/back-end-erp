package org.sid.web;

import java.util.List;

import org.sid.dao.SuiviRepository;
import org.sid.entities.Compte;
import org.sid.entities.Suivi;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ch.qos.logback.core.net.SyslogOutputStream;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/suivis")
public class SuiviRestService {
	
	@Autowired
	private SuiviRepository suiviRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Suivi> getSuivis(){
		return suiviRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{nsuivi}", method=RequestMethod.GET)
	public Suivi getSuivi(@PathVariable String nsuivi){
		return suiviRepository.findOne(nsuivi);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Suivi save(@RequestBody Suivi suivi) {
		return suiviRepository.save(suivi);
	}
	
	/*Update*/
	@RequestMapping(value="/{nsuivi}", method=RequestMethod.PUT)
	public Suivi save(@RequestBody Suivi suivi,@PathVariable String nsuivi) {
		suivi.setNumsuivi(nsuivi);
		return suiviRepository.save(suivi);
	}
	
	/*GetById*/
	/*@RequestMapping(value="/ChercherClient", method=RequestMethod.GET)
	public List<Object> getListOfCompteMc(@RequestParam(name="mc", defaultValue="") String mc){
		System.out.println(mc);
		return suiviRepository.getListOfCompte(mc);
		
	}*/
	
	/*Search*/
	@RequestMapping(value="/findByCompte", method=RequestMethod.GET)
	public Page<Object> findByCompte(
			@RequestParam(name="mc", defaultValue="") String mc,
			@RequestParam(name="page", defaultValue="0") int page,
			@RequestParam(name="size", defaultValue="5") int size){
		return suiviRepository.findByCompte("%"+mc+"%", new PageRequest(page, size));
	}
	
	@RequestMapping(value="/findByEntite", method=RequestMethod.GET)
	public Page<Object> findByEntite(
			@RequestParam(name="mc", defaultValue="") String mc,
			@RequestParam(name="page", defaultValue="0") int page,
			@RequestParam(name="size", defaultValue="5") int size){
		return suiviRepository.findByEntite("%"+mc+"%", new PageRequest(page, size));
	}
	
	@RequestMapping(value="/findBySEntite", method=RequestMethod.GET)
	public Page<Object> findBySEntite(
			@RequestParam(name="mc", defaultValue="") String mc,
			@RequestParam(name="page", defaultValue="0") int page,
			@RequestParam(name="size", defaultValue="5") int size){
		return suiviRepository.findBySEntite("%"+mc+"%", new PageRequest(page, size));
	}
	
	@RequestMapping(value="/findByClient", method=RequestMethod.GET)
	public Page<Object> findByClient(
			@RequestParam(name="mc", defaultValue="") String mc,
			@RequestParam(name="page", defaultValue="0") int page,
			@RequestParam(name="size", defaultValue="5") int size){
		return suiviRepository.findByClient("%"+mc+"%", new PageRequest(page, size));
	}
	/*End*/
	
	@RequestMapping(value="/getsuivi", method=RequestMethod.GET)
	public List<Object> getListOfSuivi(){
		
		return suiviRepository.getListOfSuivi();
		
	}
	
	

}
