package org.sid.web;

import java.util.List;

import org.sid.dao.CommentaireRepository;
import org.sid.entities.Commentaire;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/commentaires")
public class CommentaireRestService {
	
	@Autowired
	private CommentaireRepository commentaireRepository; 
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Commentaire> getCommentaires(){
		return commentaireRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{ncomm}", method=RequestMethod.GET)
	public Commentaire getCommentaire(@PathVariable Integer ncomm){
		return commentaireRepository.findOne(ncomm);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Commentaire save(@RequestBody Commentaire commentaire) {
		
		return commentaireRepository.save(commentaire);
	}
	
	

}
