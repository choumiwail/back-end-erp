package org.sid.web;

import java.util.List;

import org.sid.dao.ContactRepository;
import org.sid.entities.Contact;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/contacts")
public class ContactRestService {
	@Autowired
	private ContactRepository contactRepository ; 
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Contact> getContacts(){
		return contactRepository.findAll();
	}
	
	//GetById()
	/*GetById*/
	@RequestMapping(value="/{ncontact}", method=RequestMethod.GET)
	public Contact getContact(@PathVariable Integer ncontact){
		return contactRepository.findOne(ncontact);
	}
	
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Contact save(@RequestBody Contact ncontact){
		return contactRepository.save(ncontact);
	}

}
