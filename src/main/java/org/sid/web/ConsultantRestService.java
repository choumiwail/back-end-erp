package org.sid.web;


import java.util.List;


import org.sid.dao.ConsultantRepository;
import org.sid.entities.Consultant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/consultants")
public class ConsultantRestService {
	
	@Autowired
	private ConsultantRepository consultantRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Consultant> getConsultants(){
		return consultantRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{nconsultant}", method=RequestMethod.GET)
	public Consultant getConsultant(@PathVariable Integer nconsultant){
		return consultantRepository.findOne(nconsultant);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Consultant save(@RequestBody Consultant consultant) {
		
		return consultantRepository.save(consultant);
	}
	
	/*Update*/
	@RequestMapping(value="/{nconsultant}", method=RequestMethod.PUT)
	public Consultant save(@PathVariable Integer nconsultant,@RequestBody Consultant consultant) {
		consultant.setNconsultant(nconsultant);
		return consultantRepository.save(consultant);
	}

}

