package org.sid.web;

import java.util.List;

import org.sid.dao.AchteurRepository;
import org.sid.entities.Acheteur;
import org.sid.entities.SousEntite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/acheteurs")
public class AcheteurRestService {
	
	@Autowired
	private AchteurRepository acheteurRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Acheteur> getAcheteurs(){
		return acheteurRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{nacheteur}", method=RequestMethod.GET)
	public Acheteur getAcheteur(@PathVariable String nacheteur){
		return acheteurRepository.findOne(nacheteur);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Acheteur save(@RequestBody Acheteur acheteur) {
		
		return acheteurRepository.save(acheteur);
	}
	
	/*Update*/
	@RequestMapping(value="/{nacheteur}", method=RequestMethod.PUT)
	public Acheteur save(@PathVariable String nacheteur,@RequestBody Acheteur acheteur) {
		acheteur.setMatriculeacheteur(nacheteur);
		return acheteurRepository.save(acheteur);
	}
	
	/*@RequestMapping(value="/getAcheteur", method=RequestMethod.GET)
	public List<Object> getListOfAcheteurMc(@RequestParam(name="mc", defaultValue="") String mc){
		System.out.println(mc);
		return acheteurRepository.getListOfAcheteurs(mc);
		
	}*/

}
