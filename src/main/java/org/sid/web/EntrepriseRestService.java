package org.sid.web;

import java.util.List;


import org.sid.dao.EntrepriseRepository;
import org.sid.entities.Entreprise;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/entreprises")
public class EntrepriseRestService {
	
	@Autowired
	private EntrepriseRepository entrepriseRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Entreprise> getEntreprises(){
		return entrepriseRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{identreprise}", method=RequestMethod.GET)
	public Entreprise getEntreprise(@PathVariable Integer identreprise){
		return entrepriseRepository.findOne(identreprise);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public Entreprise save(@RequestBody Entreprise entreprise) {
		
		return entrepriseRepository.save(entreprise);
	}
	
	/*Update*/
	@RequestMapping(value="/{identreprise}", method=RequestMethod.PUT)
	public Entreprise save(@PathVariable Integer identreprise,@RequestBody Entreprise entreprise) {
		entreprise.setIdentreprise(identreprise);
		return entrepriseRepository.save(entreprise);
	}

}
