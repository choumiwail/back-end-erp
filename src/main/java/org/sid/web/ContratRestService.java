package org.sid.web;


import java.util.List;

import org.sid.dao.ContratRepository;
import org.sid.dao.OffreRepository;
import org.sid.entities.Contrat;
import org.sid.entities.Offre;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/*spring.jpa.properties.hibernate.dialect = org.hibernate.dialect.MySQL5Dialect*/

@RestController
/*Autorist to the navigator to accees query/ data */
@CrossOrigin("*")
@RequestMapping(value="/contrats")
public class ContratRestService {
	@Autowired
	private ContratRepository contratRepository;
	
	@Autowired
	private OffreRepository offreRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Contrat> getContrats(){
		return contratRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{ncontrat}", method=RequestMethod.GET)
	public Contrat getContrat(@PathVariable String ncontrat){
		return contratRepository.findOne(ncontrat);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST/*,consumes="application/json"*/)
	public  Contrat save(@RequestBody Contrat contrat){
		
		return contratRepository.save(contrat);
	}

	
	
}
