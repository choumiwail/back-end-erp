package org.sid.web;

import java.util.List;

import org.sid.dao.SousEntiteRepository;
import org.sid.entities.Compte;
import org.sid.entities.Entite;
import org.sid.entities.SousEntite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
/*Autorist to the navigator to accees query/ data */
@CrossOrigin("*")
@RequestMapping(value="/sousentites")
public class SEntiteRestService {
	
	@Autowired
	private SousEntiteRepository sousEntiteRepository;
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<SousEntite> getSentites(){
		return sousEntiteRepository.findAll();
	}
	
	/*GetById*/
	@RequestMapping(value="/{nsentite}", method=RequestMethod.GET)
	public SousEntite getSentite(@PathVariable String nsentite){
		return sousEntiteRepository.findOne(nsentite);
	}
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)
	public SousEntite save(@RequestBody SousEntite sousEntite) {
		
		return sousEntiteRepository.save(sousEntite);
	}
	
	/*Update*/
	@RequestMapping(value="/{nsentite}", method=RequestMethod.PUT)
	public SousEntite save(@PathVariable String nsentite,@RequestBody SousEntite sousentite) {
		sousentite.setNsentite(nsentite);
		return sousEntiteRepository.save(sousentite);
	}
	
	/*ListofEntite by Compte*/
	@RequestMapping(value="/getEntite", method=RequestMethod.GET)
	public List<Object> getListOfEntiteMc(@RequestParam(name="mc", defaultValue="") String mc){
		System.out.println(mc);
		return sousEntiteRepository.getListOfEntite(mc);
		
	}

}
