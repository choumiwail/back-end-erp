package org.sid.web;

import java.util.List;

import org.sid.dao.CompteRepository;
import org.sid.dao.EntiteRepository;
import org.sid.entities.Compte;
import org.sid.entities.Entite;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/entites")
public class EntiteRestService {
	@Autowired
	private EntiteRepository entiteRepository;
	@Autowired
	private CompteRepository compteRepository;
	
	
	/*GetAll*/
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Entite> getEntites(){
		return entiteRepository.findAll();
	}
	
	//GetById()
	/*GetById*/
	@RequestMapping(value="/{nentite}", method=RequestMethod.GET)
	public Entite getEntite(@PathVariable String nentite){
		return entiteRepository.findOne(nentite);
	}
	/*Search*/
	/*@RequestMapping(value="/ChercherComptes", method=RequestMethod.GET)
	public Page<Compte> chercherCom(
			@RequestParam(name="mc", defaultValue="") String mc,
			@RequestParam(name="page", defaultValue="0") int page,
			@RequestParam(name="size", defaultValue="5") int size){
		return entiteRepository.chercher("%"+mc+"%", new PageRequest(page, size));
	}*/
	
	
	/*Create*/
	@RequestMapping(value="", method=RequestMethod.POST)/*/{codecompter}*/
	public Entite save(@RequestBody Entite entite/*,@PathVariable String codecompter*/){
		//Compte compte= new Compte("C01");*/
		/*String codecompter="C01";
		
		if(codecompter == compte.getCodecompte()) {}*/
		/*Compte compte = new Compte("C01");*/
		//Compte compte = new Compte("C01");
		/*Compte compte;
		entite.setCompte(compte.getCodecompte());*/
		return entiteRepository.save(entite);
		//else return null;
	}
	
	/*Delete*/
	@RequestMapping(value="/{nentite}", method=RequestMethod.DELETE)
	public boolean supprimer(@PathVariable String nentite){
		entiteRepository.delete(nentite);
		return true;
	}
	
	/*Update*/
	@RequestMapping(value="/{nentite}", method=RequestMethod.PUT)
	public Entite save(@PathVariable String nentite,@RequestBody Entite entite) {
		entite.setNentite(nentite);;
		return entiteRepository.save(entite);
	}

}
