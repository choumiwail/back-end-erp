package org.sid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@SpringBootApplication
public class BackEndErpApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackEndErpApplication.class, args);
		/*HttpHeaders headers = new HttpHeaders();

		headers.add("Allow", "OPTIONS,HEAD,GET,POST,PUT");*/
	}
}
